﻿#include <iostream>
#include <stdlib.h>

using namespace std;

void Points(double* x, double* y, int num) {
    srand(time(NULL));
    for (int i = 0; i < num; i++) {
        x[i] = rand() % 20;
        x[i] = x[i] / 10 - 1;
        y[i] = rand() % 10;
        y[i] = y[i] / 10;
    }
}

bool isPointInTriangle(double x, double y, double x1, double y1, double x2, double y2, double x3, double y3) {
    double a = (x2 - x1) * (y - y1) - (y2 - y1) * (x - x1);
    double b = (x3 - x2) * (y - y2) - (y3 - y2) * (x - x2);
    double c = (x1 - x3) * (y - y3) - (y1 - y3) * (x - x3);
    return (a * b >= 0) && (b * c >= 0);
}

int main()
{
    double triangles[][8] = {
        {0.0, 0.0, 0.0, 1.0, 0.5, 1.0},
        {0.0, 0.0, -1.0, 0.0, 0.5, 0.5},
        {0, 0, 1, 0, 0, -1}
    };
    double count = 0.0;
    int num;
    cout << "Enter number of shots" << endl;
    cin >> num;
    double* x = new double[num];
    double* y = new double[num];
    Points(x, y, num);
    for (int i = 0; i < num; i++) {
        bool hit = false;
        for (int j = 0; j < 3; j++) {
            if (isPointInTriangle(x[i], y[i], triangles[j][0], triangles[j][1], triangles[j][2], triangles[j][3], triangles[j][4], triangles[j][5])) {
                cout << " Shot with coordinate (" << x[i] << ";" << y[i] << ") hit the target " << endl;
                count++;
                hit = true;
                break;
            }
        }
        if (!hit) {
            cout << " Shot with coordinate (" << x[i] << ";" << y[i] << ") missed the target " << endl;
        }
    }
    cout << "Hit rate: " << (count / num) * 100 << "%" << endl;
    delete[] x;
    delete[] y;
    return 0;
}


